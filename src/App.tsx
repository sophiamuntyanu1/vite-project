import { useState, useEffect } from 'react'
import './App.css'
import DevelopersList from './components/List/DevelopersList'
import { getAllDevelopers }  from './services/developerService';
import Filters  from './components/Filters/Filters';
import AddDeveloper from './components/AddDevelopers/AddDevelopers';
import { Developer } from "./models/Developer"

function App() {
  const developers = getAllDevelopers();
  const [visibleData, setVisibleData] = useState<Developer[]>([]);

  useEffect(() => {
    setVisibleData(developers)
  }, [developers]);

  const filterData = (grade?: string, specialty?: string, name?: string) => {
    let filteredDevelopers = developers;

    if (name) {
      const searchValue = name.toLowerCase();
      filteredDevelopers = filteredDevelopers.filter((developer) =>
          developer.name.toLowerCase().includes(searchValue)
      );
    }

    if (grade) {
      filteredDevelopers = filteredDevelopers.filter((developer) =>
        developer.grade.includes(grade)
      );
    }

    if (specialty) {
      filteredDevelopers = filteredDevelopers.filter((developer) => 
        developer.specialty.includes(specialty)
      );
    }
    
    setVisibleData(filteredDevelopers);
  };

  const updateDevelopers = (updatedDevelopers : Developer[]) => {
    setVisibleData([...updatedDevelopers]);
  }

  return (
    <>
      <div className="card">
      </div>
      <Filters filter = {filterData} />
      <div>
          <AddDeveloper
            onAdd={updateDevelopers}>
          </AddDeveloper>

          <DevelopersList
            developers={visibleData}
            onDelete={updateDevelopers} 
          >
          </DevelopersList>
        </div>
    </>
  )
}

export default App
