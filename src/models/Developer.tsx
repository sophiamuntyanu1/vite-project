
export interface Developer {
    id: number;
    name: string;
    grade: string;
    specialty: string;
  }

