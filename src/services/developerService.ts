import { Developer } from "../models/Developer"
  
const developers = [
    { id: 1, name: 'John Doe', grade: 'senior', specialty: 'front' },
    { id: 2, name: 'Jane Smith', grade: 'junior', specialty: 'back' },
    { id: 3, name: 'Alice Johnson', grade: 'middle', specialty: 'back' },
    { id: 4, name: 'John Smith', grade: 'senior', specialty: 'front' },
    { id: 5, name: 'Emily Johnson', grade: 'junior', specialty: 'back' },
    { id: 6, name: 'Michael Williams', grade: 'middle', specialty: 'back' },
    { id: 7, name: 'Sarah Brown', grade: 'senior', specialty: 'front' },
    { id: 8, name: 'David Wilson', grade: 'junior', specialty: 'back' },
    { id: 9, name: 'Samantha Anderson', grade: 'middle', specialty: 'back' },
    { id: 10, name: 'William Martinez', grade: 'senior', specialty: 'front' },
    { id: 11, name: 'Jennifer Taylor', grade: 'junior', specialty: 'back' },
    { id: 12, name: 'Christopher Thompson', grade: 'middle', specialty: 'back' },
    { id: 13, name: 'Jessica Davis', grade: 'senior', specialty: 'front' },
    { id: 14, name: 'Matthew White', grade: 'junior', specialty: 'back' },
    { id: 15, name: 'Elizabeth Garcia', grade: 'middle', specialty: 'back' },
    { id: 16, name: 'Nicholas Lee', grade: 'senior', specialty: 'front' },
    { id: 17, name: 'Amanda Rodriguez', grade: 'junior', specialty: 'back' },
    { id: 18, name: 'Daniel Harris', grade: 'middle', specialty: 'back' },
    { id: 19, name: 'Michelle Clark', grade: 'senior', specialty: 'front' },
    { id: 20, name: 'Joseph Young', grade: 'junior', specialty: 'back' },
    { id: 21, name: 'Ashley Scott', grade: 'middle', specialty: 'back' },
    { id: 22, name: 'Joshua Hall', grade: 'senior', specialty: 'front' },
    { id: 23, name: 'Olivia King', grade: 'junior', specialty: 'back' },
    { id: 24, name: 'Samantha Johns', grade: 'middle', specialty: 'back' },
  ];
  
  const getAllDevelopers = () => {
    return developers;
  };
  
  const updateDeveloper = (updatedDeveloper: Developer) => {
    const index = developers.findIndex(dev => dev.id === updatedDeveloper.id);
    if (index !== -1) {
      developers[index] = updatedDeveloper;
    }
  };
  
  const createDeveloper = (newDeveloper: Developer) => {
    const maxId = Math.max(...developers.map(dev => dev.id));
    const newDevWithId = { ...newDeveloper, id: maxId + 1 };
    developers.push(newDevWithId);
    return developers;
  };
  
  export  { getAllDevelopers, updateDeveloper, createDeveloper };
  