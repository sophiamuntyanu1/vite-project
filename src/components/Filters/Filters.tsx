import React, { useState } from 'react';

import { filtersGrade } from '../../models/Filters';
import { filtersSpecialty } from '../../models/FiltersSpecialty';
import './style.sass';
type FilterProps = {
    filter: (grade?: string | undefined, specialty?: string | undefined, name?: string | undefined) => void
};

function Filters({ filter }: FilterProps) {
   
    const [gradeValue, setGradeValue] = useState("");
    const [specialtyValue, setSpecialtyValue] = useState("");
    const [nameValue, setNameValue] = useState("");
   
    const handleGradeChange = (grade: any) => {
        setGradeValue(grade)
        filter(grade, specialtyValue, nameValue)
    };

    const handleSpecialtyChange = (specialty: any) => {
        setSpecialtyValue(specialty)
        filter(gradeValue, specialty, nameValue)
    };
    
    const handleNameChange = (e: any) => {
        const searchValue = e.target.value;
        setNameValue(searchValue)
        filter(gradeValue, specialtyValue, searchValue)
    };

    const resetAllFilters = () => {
        setGradeValue("");
        setSpecialtyValue("");
        setNameValue("");
        filter("", "", "");
    };

    return (
        <div className='filters'>
            <div className='filtersGrade'>
            <div className='filtersGradeTitle'>Choose a grade</div>
            {filtersGrade.map(({ id, grade }) => {
                return (
                    <>
                     <button
                        key={id}
                        onClick={() => handleGradeChange(grade)}
                    >
                        <div>
                            <div>{grade}</div>
                        </div>
                    </button>
                   
                    </>
                );
            })}
            </div>

            <div className='filtersSpecialty'>
                <div className='filtersSpecialtyTitle'>Choose a specialty</div>
                {filtersSpecialty.map(({ id, specialty }) => {
                    return (
                        <>
                        <button
                            key={id}
                            onClick={() => handleSpecialtyChange(specialty)}
                        >
                            <div>
                                <div>{specialty}</div>
                            </div>
                        </button>
                    
                        </>
                    );
                })}
            </div>

            <input type="text" placeholder="Поиск по имени" onChange={handleNameChange} />
            <button onClick={resetAllFilters}>Сбросить все фильтры</button>
        </div>
    );
}

export default Filters;
